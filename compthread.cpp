#include "compthread.h"

#define LIMIT 4
#define ITERS 255

compThread::compThread(QObject *parent) : QThread(parent)
{

}

compThread::~compThread(){
    mutex.lock();
    abort = true;
    condition.wakeOne();
    mutex.unlock();

    wait();
}

void compThread::render(double centerRe, double centerIm, double scalePoint){               // this function is called from outside of this class to start rendering picture
    mutex.lock();
    this->centerRe = centerRe;
    this->centerIm = centerIm;
    this->scalePoint = scalePoint;

    if (!isRunning()){
        start(LowPriority);
    } else {
        restart = true;
        condition.wakeOne();
    }
    mutex.unlock();
}

void compThread::setWidth(int width){
    mutex.lock();
    this->width = width;
    mutex.unlock();
}

void compThread::setHeight(int height){
    mutex.lock();
    this->height = height;
    mutex.unlock();
}

void compThread::setPalette(const QString &name){
    mutex.lock();
    this->pltName = name;
    mutex.unlock();
}

void compThread::setIter(int iter)
{
    mutex.lock();
    this->iters = iter;
    mutex.unlock();
}

void compThread::run(){
    forever {
        if (abort){
            return;
        }

        mutex.lock();
        const double centerRe = this->centerRe;
        const double centerIm = this->centerIm;
        const double scalePoint = this->scalePoint;

        const double width = this->width;
        const double height = this->height;
        mutex.unlock();

        const double d_re = scalePoint / (width / 2);
        const double d_im = scalePoint / (height / 2);
        const double start_re = centerRe - scalePoint;
        const double start_im = centerIm - scalePoint;

        QImage image(width, height, QImage::Format_RGB32);
        bool def;
        if (pltName == "default"){
            def = true;
        } else {
            def = false;
        }
        Palette plt(pltName);

        for (int i = 0; i < height; ++i){
            double y = start_im + d_im * i;
            if (restart){
                break;
            }

            for (int j = 0; j < width; ++j){
                double x = start_re + d_re * j;

                int iter = computeIter(x, y, iters);
                if (!def){
                    image.setPixel(j, i, plt.at(255 - (int)(255*iter/iters)));
                } else {
                    int c = 255 - 255*iter/iters;
                    image.setPixel(j, i, qRgb(c, c, c));
                }
            }
        }

        if (!restart)
        {
            emit finishedComp(image);
        }

        mutex.lock();
        if (!restart){
            condition.wait(&mutex);
        }
        restart = false;
        mutex.unlock();
    }
}

int computeIter(const double x, const double y, int n){                 // computes number of iterations for the given point
    double z_re = x;
    double z_im = y;

    for (int iter = 0; iter < n; ++iter){
        if (absolute(z_re, z_im) > LIMIT){
            return iter;
        }

        complex_square(&z_re, &z_im);
        z_re += x;
        z_im += y;
    }
    return n;
}

void complex_square(double *re, double *im)                             // second power of the given complex number
{
    double a = (*re) * (*re) - (*im) * (*im);
    double b = 2 * (*re) * (*im);

    *re = a;
    *im = b;
}

double absolute(const double x, const double y){                        // absolute value of complex number
    return x*x + y*y;
}
