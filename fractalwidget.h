#ifndef FRACTALWIDGET_H
#define FRACTALWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QWheelEvent>
#include "compthread.h"

class fractalWidget : public QWidget
{
    Q_OBJECT
public:
    explicit fractalWidget(QWidget *parent = nullptr);
    void setupWidget();

    void setWidth(int value);
    void setHeight(int value);

    void sendPalette(const QString &name);
    inline QPixmap pixmapRet() {return pixmap;}
public slots:
    void changePixX(int value);
    void changePixY(int value);
    void setIter(int iter);

protected:
    void paintEvent(QPaintEvent *) override;
    void resizeEvent(QResizeEvent *) override;
#if QT_CONFIG(wheelevent)
    void wheelEvent(QWheelEvent *event) override;
#endif
private:
    compThread thread;

    QPixmap pixmap;
    double centerRe;
    double centerIm;
    double scalePoint;

    int pixX = 0;
    int pixY = 0;

private slots:
    void updateImage(const QImage &image);
};

#endif // FRACTALWIDGET_H
