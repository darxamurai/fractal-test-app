Fractal test app

This app allows you to generate Mandelbrot Set based on color palette, resolution and number of iterations.
After changing one of parameters fractal is rendering automatically (with higher number of iterations this can take a while, wait).
Also you can zoom fractal using mouse wheel (this also can take a while on higher number).
When resolution is bigger than the default, use sliders to move image.
You can save your image as png or jpg using the buttons.

To launch app you need to have installed QtCreator with Desktop kit.
Palettes in plt format need to be added to generated build folder.
Images are saved to the same folder.
