#include "palette.h"

Palette::Palette(const QString &name) : QFile(name)
{
    processFile();
}

bool Palette::processFile(){
    if (!this->open(QIODevice::ReadOnly | QIODevice::Text)){
        return false;
    }

    QTextStream in(this);
    while (!in.atEnd()){
        QString line = in.readLine();

        colors.append(parseString(line));
    }
    return true;
}

QRgb parseString(QString &line){
    int c[3];
    QRgb color;

    for (int i = 0; i < 3; ++i){
        c[i] = line.split(";")[i].toInt();
    }
    color = qRgb(c[0],c[1],c[2]);
    return color;
}

QRgb Palette::at(int i){
    return colors[i];
}
