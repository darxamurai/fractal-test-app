#include "window.h"
#include "ui_window.h"

Window::Window(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Window)
{
    ui->setupUi(this);

    ui->widthSpinBox->setValue(ui->imageWidget->width());
    ui->heightSpinBox->setValue(ui->imageWidget->height());

    ui->imageWidget->setupWidget();

    ui->paletteBox->addItem("default");

    QDir dir = QDir();                                          // open the build directory with palettes
    QStringList palettes = palettesInDir(dir);
    for (int i = 0; i < palettes.size(); ++i){
        ui->paletteBox->addItem(palettes[i]);
    }

    connect(ui->paletteBox, &QComboBox::currentTextChanged, this, &Window::itemChoosed);

    connect(ui->widthSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &Window::widthChanged);
    connect(ui->heightSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &Window::heightChanged);

    connect(ui->jpgButton, &QPushButton::clicked, this, &Window::saveJpg);
    connect(ui->pngButton, &QPushButton::clicked, this, &Window::savePng);

    connect(ui->horizontalScrollBar, &QAbstractSlider::valueChanged, ui->imageWidget, &fractalWidget::changePixX);
    connect(ui->verticalScrollBar, &QAbstractSlider::valueChanged, ui->imageWidget, &fractalWidget::changePixY);

    connect(ui->depthSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), ui->imageWidget, &fractalWidget::setIter);
}

Window::~Window()
{
    delete ui;
}

void Window::widthChanged(int value){
    ui->imageWidget->setWidth(value);
    if (value >= 780){
        ui->horizontalScrollBar->setMaximum(value - ui->horizontalScrollBar->pageStep());
    } else {
        ui->horizontalScrollBar->setMaximum(0);
    }
}

void Window::heightChanged(int value){
    ui->imageWidget->setHeight(value);
    if (value >= 480){
        ui->verticalScrollBar->setMaximum(value - ui->verticalScrollBar->pageStep());
    } else {
        ui->verticalScrollBar->setMaximum(0);
    }
}

void Window::itemChoosed(const QString &name){
    ui->imageWidget->sendPalette(name);
}

void Window::saveJpg(bool){
    QPixmap pixmap = ui->imageWidget->pixmapRet();
    pixmap.save(ui->fileNameEdit->text() + ".jpg");
}

void Window::savePng(bool){
    QPixmap pixmap = ui->imageWidget->pixmapRet();
    pixmap.save(ui->fileNameEdit->text() + ".png");
}

QStringList Window::palettesInDir(QDir dir){
    QStringList filters;
    filters << "*.plt";
    dir.setNameFilters(filters);
    return dir.entryList();
}
