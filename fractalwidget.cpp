#include "fractalwidget.h"

const double defaultCenterRe = -0.5;
const double defaultCenterIm = 0.0;
const double defaultScalePoint = 1.5;

fractalWidget::fractalWidget(QWidget *parent) :
    QWidget(parent), centerRe(defaultCenterRe), centerIm(defaultCenterIm), scalePoint(defaultScalePoint)
{
    connect(&thread, &compThread::finishedComp, this, &fractalWidget::updateImage);
}

void fractalWidget::updateImage(const QImage &image){
    pixmap = QPixmap::fromImage(image);
    update();
}

void fractalWidget::paintEvent(QPaintEvent * ){
    QPainter painter(this);

    if (pixmap.isNull()){
        return;
    }
    painter.drawPixmap(0, 0, this->width(), this->height(), pixmap, pixX, pixY, 780, 480);
}

void fractalWidget::setWidth(int value){
    thread.setWidth(value);

    thread.render(centerRe, centerIm, scalePoint);
}

void fractalWidget::setHeight(int value){
    thread.setHeight(value);

    thread.render(centerRe, centerIm, scalePoint);
}

void fractalWidget::setIter(int iter)
{
    thread.setIter(iter);

    thread.render(centerRe, centerIm, scalePoint);
}

void fractalWidget::changePixX(int value){          // changes what part of picture is rendered
    if (value > 0){
        pixX = value;
    } else {
        pixX = 0;
    }
    update();
}

void fractalWidget::changePixY(int value){          // changes what part of picture is rendered
    if (value > 0){
        pixY = value;
    } else {
        pixY = 0;
    }
    update();
}

void fractalWidget::resizeEvent(QResizeEvent *){
    thread.setWidth(this->width());
    thread.setHeight(this->height());

    thread.render(centerRe, centerIm, scalePoint);
}

void fractalWidget::wheelEvent(QWheelEvent *event){                                             // function for zooming
    double re = (2*scalePoint/this->width())*event->position().x() + (centerRe - scalePoint);
    double im = (2*scalePoint/this->height())*event->position().y() + (centerIm - scalePoint);

    if (event->angleDelta().y() > 0){
        centerRe = centerRe + (re - centerRe)/10;
        centerIm = centerIm + (im - centerIm)/10;
        scalePoint = scalePoint - scalePoint/10;
    } else {
        centerRe = centerRe - (re - centerRe)/10;
        centerIm = centerIm - (im - centerIm)/10;
        scalePoint = scalePoint + scalePoint/10;
    }

    thread.render(centerRe, centerIm, scalePoint);
}

void fractalWidget::setupWidget(){
    thread.setWidth(this->width());
    thread.setHeight(this->height());
    thread.setIter(1);

    thread.render(centerRe, centerIm, scalePoint);
}

void fractalWidget::sendPalette(const QString &name){
    thread.setPalette(name);
    thread.render(centerRe, centerIm, scalePoint);
}
