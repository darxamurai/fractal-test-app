#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QDebug>
#include <QDir>
#include "fractalwidget.h"
#include "compthread.h"
#include "palette.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE

class Window : public QWidget
{
    Q_OBJECT

public:
    Window(QWidget *parent = nullptr);
    ~Window();

private:
    Ui::Window *ui;

    QStringList palettesInDir(QDir dir);
private slots:
    void widthChanged(int value);
    void heightChanged(int value);

    void itemChoosed(const QString &name);
    void saveJpg(bool);
    void savePng(bool);
};
#endif // WINDOW_H
