#ifndef COMPTHREAD_H
#define COMPTHREAD_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QImage>
#include <QDebug>
#include "palette.h"

class compThread : public QThread
{
    Q_OBJECT
public:
    compThread(QObject *parent = nullptr);
    ~compThread();

    void render(double centerRe, double centerIm, double scalePoint);

    void setWidth(int width);
    void setHeight(int height);
    void setPalette(const QString &name);
    void setIter(int iter);
signals:
    void finishedComp(const QImage &image);

protected:
    void run() override;

private:


    QMutex mutex;
    QWaitCondition condition;
    bool abort = false;
    bool restart = false;
    QString pltName = "default";

    double centerRe;
    double centerIm;
    double scalePoint;

    int width;
    int height;

    int iters;
};

int computeIter(const double x, const double y, int n);
void complex_square(double *re, double *im);
double absolute(const double x, const double y);

#endif // COMPTHREAD_H
