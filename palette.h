#ifndef PALETTE_H
#define PALETTE_H

#include <QFile>
#include <QRgb>
#include <QTextStream>
#include <QDebug>

class Palette : public QFile
{
    Q_OBJECT
public:
    Palette(const QString &name);
    QRgb at(int i);
private:
    QList<QRgb> colors;
    bool processFile();
};

QRgb parseString(QString &line);

#endif // PALETTE_H
